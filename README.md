# obs-scripts

Some small OBS scripts that I've made.

## quodlibet.lua

Displays the current playing track from Quod Libet in a text source.

The format string uses the same keys as in the Quod Libet current playing file,
surrounded by `#`. An example: `Now playing: #title# by #artist"`

