-- vim: set ts=2 sw=2 expandtab:

local item_settings = {}
item_settings.source = "quodlibet"
item_settings.format = "Now playing: #title# by #artist#"

-- Try several paths to possible `quodlibet/current` files, and return the
-- first one that exists
function get_quodlibet_file()
  local tries = {}
  if item_settings.current_file ~= "" and item_settings.current_file ~= nil then
    tries[#tries + 1] = item_settings.current_file
  end

  if os.getenv("XDG_CONFIG_PATH") ~= nil then
    tries[#tries + 1] = os.getenv("XDG_CONFIG_PATH") .. "/quodlibet/current"
  end

  if os.getenv("HOME") ~= nil then
    tries[#tries + 1] = os.getenv("HOME") .. "/.config/quodlibet/current"
  end

  for k, v in ipairs(tries) do
    local f = io.open(v, "rb")
    if f then f:close() end
    if f ~= nil then
      return v
    end
  end

  return nil
end

-- Return a table of key/value pairs from the Quod Libet current playing file.
function get_quodlibet_current()
  local items = {}
  local file = get_quodlibet_file()
  if file == nil then
    items = {}
    items.absent = true
    return items
  end

  for line in io.lines(file) do
    for k, v in string.gmatch(line, "([A-Za-z0-9~#]+)=(.*)") do
      items[k] = v
    end
  end

  return items
end

-- Perform the actual formatting of the output
function item_format()
  local data = item_settings.format
  local current = get_quodlibet_current()
  if current.absent == true then return "" end

  for k, v in pairs(current) do
    data = string.gsub(data, "#"..k.."#", v)
  end

  return data
end

-- Update the text in the source given in the config
function item_update()
  local source = obslua.obs_get_source_by_name(item_settings.source)
  if source == nil then return end

  local text = item_format(get_quodlibet_current())

  local settings = obslua.obs_data_create()
  obslua.obs_data_set_string(settings, "text", text)
  obslua.obs_source_update(source, settings)
  obslua.obs_data_release(settings)
  obslua.obs_source_release(source)
end

function script_properties()
  local props = obslua.obs_properties_create()
  obslua.obs_properties_add_text(props, "current_file", "Path to .quodlibet/current (leave empty to try defaults)", obslua.OBS_TEXT_DEFAULT)
  obslua.obs_properties_add_text(props, "source", "Source name", obslua.OBS_TEXT_DEFAULT)
  obslua.obs_properties_add_text(props, "format", "Format of output", obslua.OBS_TEXT_MULTILINE)
  return props
end

function script_update(settings)
  obslua.timer_remove(item_update)
  script_load(settings)
end

function script_load(settings)
  item_settings.current_file = obslua.obs_data_get_string(settings, "current_file")
  item_settings.source = obslua.obs_data_get_string(settings, "source")
  item_settings.format = obslua.obs_data_get_string(settings, "format")

  obslua.timer_add(item_update, 500)
end

function script_description()
  return "Display the currently playing track from Quod Libet."
end
